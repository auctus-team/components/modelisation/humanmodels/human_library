NB : The size taken by Opensim is about 200 Mb. 

# Installation from the latest downloable version (ONLY WITH PYTHON 3.4!!!).

* Modify the variables HOME_PATH and FOLDER_PATH in install_opensim and install_opensim_gui.sh.
* Put the folder opensim_install where you want Opensim to be installed.
* Launch the script install_opensim.sh to install Opensim.
* Next, follow the instructions of "About installing opensim-core from source (Ubuntu 18.04, Python 3.6.9, Matlab R2020b)", from "Add to ~/.bashrc" to "Try the command into the opensim-core-source/opensim-core-build folder".
Those commands will 
* Should you feel very lucky, you may try to install Opensim GUI with install_opensim_gui.sh.


export LIBRARY_PATH=$OPENSIM_HOME2/sdk/lib:$OPENSIM_HOME2/opensim-core/sdk/Simbody/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$OPENSIM_HOME2/sdk/lib:$OPENSIM_HOME2/opensim-core/sdk/Simbody/lib:$LIBRARY_PATH
export PATH=$OPENSIM_HOME2/bin:$PATH

* Go to opensim-core-install/sdk/Python, execute the script setup.py : 

~~~
sudo python3 setup.py install
~~~

Test if Opensim is working into python with the command : 

~~~
python3
import opensim
~~~


# About installing opensim-core from source

## For Ubuntu 18.04, Python 3.6.9, Matlab R2020b :

Please follow the instructions of : https://github.com/opensim-org/opensim-core, starting from "On Ubuntu using Unix Makefiles".

Do not use the "For the impatient" part (which is false), and install the following dependencies : 

~~~
sudo apt-get install cmake-gui doxygen git swig openjdk-8-jdk python-dev python-numpy wget build-essential libtool autoconf pkg-config gfortran 
~~~

# NB : install cmake-gui : https://www.programmersought.com/article/58615438357/

Then, follows the instructions starting from "Download the OpenSim-Core source code" until the 4th step of the "Build and Install" part.
You may install all the dependencies at the "[Optional] Superbuild: download and build OpenSim dependencies" step. The cmake-gui conditions for opensim-core-build are displayed here : https://imgur.com/MdgyUZG


Do the command "ctest -j8". There should be some bugs with Matlab / Python programs, which is fine. Nevertheless, do the next step of "Build and Install" (make -j8 install).

At this step, let's suppose the folder where opensim is installed is called "opensim-core-install", its path is "~/opensim-core-source/opensim-core-install", the path to the building files is "~/opensim-core-source/opensim-core-build", and the path to the files from github is "~/opensim-core-source/opensim-core". Do the following commands (inspired from https://simtk.org/plugins/phpBB/viewtopicPhpbb.php?f=91&t=7165&p=0&start=10&view=&sid=027a6b00fc209f82b9e80125bb6c63e5) :

* Add to ~/.bashrc : 

~~~
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

export OPENSIM_HOME2="< PATH OF ~ >/opensim-core-source/opensim-core-install"

export LIBRARY_PATH=$OPENSIM_HOME2/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$OPENSIM_HOME2/lib:$LD_LIBRARY_PATH
export PATH=$OPENSIM_HOME2/bin:$PATH

export LD_PRELOAD="/usr/lib/x86_64-linux-gnu/libstdc++.so.6:/usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.7.1:/usr/lib/x86_64-linux-gnu/blas/libblas.so.3.7.1"
~~~

And then source ~/.bashrc .

* Go to opensim-core-install/sdk/Python, execute the script setup.py : 

~~~
sudo python3 setup.py install
~~~

Test if Opensim is working into python with the command : 

~~~
python3
import opensim
~~~

* Go to opensim-core-install/Resources/Code/Matlab, execute the script configureOpenSim.m with Matlab.

* Copy the file "WalkerModelTerrain.osim" from "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/Model" to "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/UserFunctions"

*  Check the local variables via the "locale" command. 
If these variables do not match "en_US.UTF-8", do the following commands and then restart the session : 

~~~
sudo locale-gen "en_US.UTF-8"
sudo dpkg-reconfigure locales 
~~~

* Try the command into the opensim-core-source/opensim-core-build folder : 

~~~
ctest -j8
~~~

If everything is good, congratulations, Opensim is operational! 

## (BETA) For Ubuntu 20.04, Python >=3.8.10, Matlab R2020b, Opensim 4.3-2021-07-29 :

### Option 1 

- The install will take a HUGE time. Count at least 2 hours for all the necessary steps.

- PLEASE NOTE IT IS REALLY EASY TO FREEZE YOUR COMPUTER WITH THE BUILDING STEP, EVEN WITH 16 GO OF RAM!!!

(Partly based on : https://github.com/opensim-org/opensim-core starting from "On Ubuntu using Unix Makefiles"; https://simtk.org/plugins/phpBB/viewtopicPhpbb.php?f=91&t=12807&p=37982&start=0&view= ).

* Install the following dependencies : 

~~~
sudo apt-get install cmake-gui cmake-qt-gui doxygen git openjdk-8-jdk wget build-essential libtool autoconf pkg-config gfortran libqt5quickcontrols2-5 libqt5multimedia5 libqt5webengine5 libqt5quick5 libqt5qml5 cmake-curses-gui freeglut3-dev libxi-dev libxmu-dev liblapack-dev python-dev texlive
~~~

* Then, writes into your ~/.bashrc : 

~~~
### For Opensim install
alias python=python3
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
~~~

* Install the latest version of Swig (>= 4.0.2), from here : http://www.swig.org/

- Extract the .zip file you will have downloaded, and follow the instructions from the readme inside the .zip.

- Go to /usr/bin. If there is no file called "swig4.0", go to the folder where you extracted your .zip file; copy the swig file with the following command : 

~~~
sudo cp swig /usr/bin/swig4.0
~~~

- Check that the install is correct with the following command : 

~~~
swig -version
~~~

Which should returns a version superior or equal to 4.0.2. 

* Creates a folder in which you will clone and install all the necessary folders. 
Here, it will be ~/opensim_install_folder. The absolute path is home/elandais/opensim_install_folder.

The commands will be the following : 

~~~
cd ~
mkdir opensim_install_folder
cd opensim_install_folder
git clone https://github.com/opensim-org/opensim-core.git
~~~

* Executes cmake-gui into ~/opensim_install_folder. 

- For Where is the source code, put "/home/elandais/opensim_install_folder/opensim-core/dependencies"
For Where to build the binairies, put "/home/elandais/opensim_install_folder/opensim-core-dependencies-build"

- Click "Configure", click "Finish".

- On CMAKE_BUILD_TYPE, put RelWithDebInfo.
You can select all the dependencies (= SUPERBUILD_[...]).

My own configuration can be seen here : https://imgur.com/vLf7X1A. 

- Click "Configure", then "Generate".

* Go to /home/elandais/opensim_install_folder/opensim-core-dependencies-build. 

Executes : 

~~~
make -j8
~~~

* Again, executes cmake-gui into ~/opensim_install_folder. 

- For Where is the source code, put "/home/elandais/opensim_install_folder/opensim-core"
For Where to build the binairies, put "/home/elandais/opensim_install_folder/opensim-core-build"

- Click "Configure", click "Finish".

- Select BUILD_JAVA_WRAPPING, BUILD_PYTHON_WRAPPING, set "SIMBODY_HOME" to "/home/elandais/opensim_install_folder/opensim_dependencies_install/simbody".

- Click "Configure".

- Set "Matlab_ROOT_DIR" with "/usr/local/MATLAB/R2020b".

- Click "Configure", then click again on "Configure".

- Click on "Generate".

My own configuration is the following : https://imgur.com/a/83GdOj5

* Go to /home/elandais/opensim_install_folder/opensim-core-build

Executes : 

~~~
make -j4
~~~

THIS STEP WILL TAKE A LOT OF TIME, AND HAS HUGE CHANCES TO CRASH YOUR COMPUTER. BE WARNED AND PREPARED TO RELAUNCH THE COMMAND IF NECESSARY.

* Modify your ~/.bashrc file with the following : 

~~~
export OPENSIM_HOME2="$HOME/opensim_install_folder/opensim-core-install"
export OPENSIM_DEPENDENCIES2="$HOME/opensim-install_folder/opensim_dependencies_install"
export LIBRARY_PATH=$OPENSIM_HOME2/lib:$OPENSIM_DEPENDENCIES2/adol-c/lib64:$OPENSIM_DEPENDENCIES2/ipopt/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$OPENSIM_HOME2/lib:$OPENSIM_DEPENDENCIES2/adol-c/lib64:$OPENSIM_DEPENDENCIES2/ipopt/lib:$LD_LIBRARY_PATH
export PATH=$OPENSIM_HOME2/bin:$PATH
export PYTHONPATH=$OPENSIM_HOME2/lib/python3.8/site-packages:$PYTOHNPATH

export LD_PRELOAD="/usr/lib/x86_64-linux-gnu/libstdc++.so.6:/usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0:/usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0"
~~~

With OPENSIM_HOME2 the path to the install of Opensim, and OPENSIM_DEPENDENCIES2 the path to the install of the dependencies of Opensim.

And do 

~~~
source ~/.bashrc
sudo ldconfig
~~~

* Go to /home/elandais/opensim_install_folder/opensim-core-build

Executes : 

~~~
make -j4 install
~~~

* Go to opensim-core-install/lib/python3.8/site-packages, execute the script setup.py : 

~~~
sudo python3 setup.py install
~~~

Test if Opensim is working into python with the command : 

~~~
python3
import opensim
~~~


* Go to opensim-core-install/share/doc/OpenSim/Code/Matlab, execute the script configureOpenSim.m with Matlab.

* Copy the file "WalkerModelTerrain.osim" from "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/Model" to "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/UserFunctions"

* Add to Matlab path the folder : opensim_install_folder/opensim-core-install/share/doc/OpenSim/Code/Matlab/Moco

*  Check the local variables via the "locale" command. 
If these variables do not match "en_US.UTF-8", do the following commands and then restart the session : 

~~~
sudo locale-gen "en_US.UTF-8"
sudo dpkg-reconfigure locales 
~~~

* Try the command into the opensim-core-source/opensim-core-build folder : 

~~~
ctest -j8
~~~

If everything is good, congratulations, Opensim is operational! 

NB : for now, 2 tests are still not working : 

120 - testCommandLineInterface : it seems to be linked to an issue with the order of the variables of the run-tool command. The output for the command called by the program is the following : 

~/opensim_install_folder/opensim-core-build$ opensim-cmd -L "/home/elandais/opensim_install_folder/opensim-core-build/libosimActuators.so" run-tool -h
~~~
[info] Loaded library /home/elandais/opensim_install_folder/opensim-core-build/libosimActuators.so
Run a tool (e.g., Inverse Kinematics) from an XML setup file.

Usage:
  opensim-cmd [options]... run-tool <setup-xml-file>
  opensim-cmd run-tool -h | --help

Options:
  -L <path>, --library <path>  Load a plugin.
  -o <level>, --log <level>  Logging level.

Description:
  The Tool to run is detected from the setup file you provide. Supported tools
  include the following:
  
            Scale
            Inverse Kinematics           (IK)
            Inverse Dynamics             (ID)
            Residual Reduction Algorithm (RRA)
            Computed Muscle Control      (CMC)
            Forward                      
            Analyze
            MocoStudy

  This command will also recognize tools from plugins.

  Use `opensim-cmd print-xml` to generate a template <setup-xml-file>.

Examples:
  opensim-cmd run-tool CMC_setup.xml
  opensim-cmd -L C:\Plugins\osimMyCustomForce.dll run-tool CMC_setup.xml
  opensim-cmd --library ../plugins/libosimMyPlugin.so run-tool Forward_setup.xml
  opensim-cmd --library=libosimMyCustomForce.dylib run-tool CMC_setup.xml

malloc_consolidate(): invalid chunk size
Aborted (core dumped)
~~~

And the output of a slightly different call is the following : 

~/opensim_install_folder/opensim-core-build$ opensim-cmd run-tool -L "/home/elandais/opensim_install_folder/opensim-core-build/libosimActuators.so" -h
~~~
Run a tool (e.g., Inverse Kinematics) from an XML setup file.

Usage:
  opensim-cmd [options]... run-tool <setup-xml-file>
  opensim-cmd run-tool -h | --help

Options:
  -L <path>, --library <path>  Load a plugin.
  -o <level>, --log <level>  Logging level.

Description:
  The Tool to run is detected from the setup file you provide. Supported tools
  include the following:
  
            Scale
            Inverse Kinematics           (IK)
            Inverse Dynamics             (ID)
            Residual Reduction Algorithm (RRA)
            Computed Muscle Control      (CMC)
            Forward                      
            Analyze
            MocoStudy

  This command will also recognize tools from plugins.

  Use `opensim-cmd print-xml` to generate a template <setup-xml-file>.

Examples:
  opensim-cmd run-tool CMC_setup.xml
  opensim-cmd -L C:\Plugins\osimMyCustomForce.dll run-tool CMC_setup.xml
  opensim-cmd --library ../plugins/libosimMyPlugin.so run-tool Forward_setup.xml
  opensim-cmd --library=libosimMyCustomForce.dylib run-tool CMC_setup.xml
~~~

### Option 2 (many thanks to LAISNE Gautier for testing) :

(Partly based on : https://github.com/opensim-org/opensim-core starting from "On Ubuntu using Unix Makefiles"; https://simtk.org/plugins/phpBB/viewtopicPhpbb.php?f=91&t=12807&p=37982&start=0&view= ).

* Install the following : 

~~~
sudo add-apt-repository --yes ppa:george-edison55/cmake-3.x
sudo apt-add-repository --yes ppa:fenics-packages/fenics-exp
sudo apt-get update
sudo apt-get –yes install cmake cmake-curses-gui cmake-qt-gui clang-3.6 doxygen git openjdk-8-jdk wget build-essential libtool autoconf pkg-config gfortran libqt5quickcontrols2-5 libqt5multimedia5 libqt5webengine5 libqt5quick5 libqt5qml5 cmake-curses-gui freeglut3-dev libxi-dev libxmu-dev liblapack-dev python-dev texlive swig4.0 pip3
sudo rm -f /usr/bin/cc /usr/bin/c++
sudo ln -s /usr/bin/clang-3.6 /usr/bin/cc
sudo ln -s /usr/bin/clang++-3.6 /usr/bin/c++
pip3 install numpy
~~~

* Then, writes into your ~/.bashrc : 

~~~
### For Opensim install
alias python=python3
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

export OPENSIM_HOME2="$HOME/opensim_install_folder/opensim-core-install"
export OPENSIM_DEPENDENCIES2="$HOME/opensim-install_folder/opensim_dependencies_install"
export LIBRARY_PATH=$OPENSIM_HOME2/lib:$OPENSIM_DEPENDENCIES2/adol-c/lib64:$OPENSIM_DEPENDENCIES2/ipopt/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$OPENSIM_HOME2/lib:$OPENSIM_DEPENDENCIES2/adol-c/lib64:$OPENSIM_DEPENDENCIES2/ipopt/lib:$LD_LIBRARY_PATH
export PATH=$OPENSIM_HOME2/bin:$PATH
export PYTHONPATH=$OPENSIM_HOME2/lib/python3.8/site-packages:$PYTOHNPATH

export LD_PRELOAD="/usr/lib/x86_64-linux-gnu/libstdc++.so.6:/usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0:/usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0"
~~~

With OPENSIM_HOME2 the path to the install of Opensim, and OPENSIM_DEPENDENCIES2 the path to the install of the dependencies of Opensim.

* Creates a folder in which you will clone and install all the necessary folders. 
Here, it will be ~/opensim_install_folder. The absolute path is home/elandais/opensim_install_folder.

The commands will be the following : 

~~~
source ~/.bashrc                                          
mkdir -p $OPENSIM_HOME2
cd $OPENSIM_HOME2
cd ..
git clone https://github.com/opensim-org/opensim-core.git
mkdir opensim_dependencies_build
cd opensim_dependencies_build
cmake .. \
      -DCMAKE_INSTALL_PREFIX=$OPENSIM_DEPENDENCIES2 \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo
make -j8
cd ..
mkdir opensim_build
cd opensim_build
cmake .. \
      -DCMAKE_INSTALL_PREFIX=$OPENSIM_HOME2 \
      -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DOPENSIM_DEPENDENCIES_DIR=$OPENSIM_DEPENDENCIES2 \
      -DBUILD_PYTHON_WRAPPING=ON \
      -DBUILD_JAVA_WRAPPING=ON \
      -DWITH_BTK=ON
make -j8
~~~

* Go to opensim-core-install/lib/python3.8/site-packages, execute the script setup.py : 

~~~
sudo python3 setup.py install
~~~

Test if Opensim is working into python with the command : 

~~~
python3
import opensim
~~~

* Go to opensim-core-install/share/doc/OpenSim/Code/Matlab, execute the script configureOpenSim.m with Matlab.

* Copy the file "WalkerModelTerrain.osim" from "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/Model" to "~/opensim-core-source/opensim-core-build/Matlab/Dynamic_Walker_Challenge/UserFunctions"

* Add to Matlab path the folder : opensim_install_folder/opensim-core-install/share/doc/OpenSim/Code/Matlab/Moco

*  Check the local variables via the "locale" command. 
If these variables do not match "en_US.UTF-8", do the following commands and then restart the session : 

~~~
sudo locale-gen "en_US.UTF-8"
sudo dpkg-reconfigure locales 
~~~

* Try the command into the opensim-core-source/opensim-core-build folder : 

~~~
ctest -j8
~~~

* If everything is good, do : 

~~~
make -j8 install
~~~

Then congratulations, Opensim is operational! 

# How to use it

## CMakeList configuration 

* First, you may try this version (which is shorter) :

~~~
cmake_minimum_required(VERSION 3.2)
project(MonProjet)

find_package(
  catkin REQUIRED COMPONENTS)

find_package(OpenSim REQUIRED)

add_executable(main <path_to_main>)
target_link_libraries(main PUBLIC ${catkin_LIBRARIES} ${OpenSim_LIBRARIES})

~~~

Should you encounter some bugs, please try this version : 

~~~
cmake_minimum_required(VERSION 3.2)
project(MonProjet)

## Settings.
## ---------
set(TARGET exampleMain CACHE TYPE STRING)

## OpenSim uses C++11 language features.
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set (CMAKE_INSTALL_PREFIX "~/opensim_install")
set (OPENSIM_INSTALL_DIR "~/opensim_install_folder/opensim-core-install")

## Find and hook up to OpenSim.
## ----------------------------
find_package(OpenSim REQUIRED PATHS "${OPENSIM_INSTALL_DIR}")

## Configure this project.
## -----------------------
file(GLOB SOURCE_FILES *.h *.cpp)

add_executable(${TARGET} ${SOURCE_FILES})

target_link_libraries(${TARGET} ${OpenSim_LIBRARIES})

## This block copies the additional files into the running directory
## For example vtp, obj files. Add to the end for more extensions
file(GLOB DATA_FILES *.vtp *.obj)
foreach(dataFile ${DATA_FILES})
    add_custom_command(
        TARGET ${TARGET}
        COMMAND ${CMAKE_COMMAND}
        ARGS -E copy
        ${dataFile}
        ${CMAKE_BINARY_DIR})
endforeach(dataFile)
~~~

## Cmake building

Please check https://gitlab.inria.fr/auctus-team/people/gautierlaisne/public/opensim-in-cpp-simple-example-project/-/blob/main/README.md

You may also add, in order to get autocompletion for Opensim in VSCode, into .vscode/c_cpp_properties.json : 

~~~
"includePath": [
  <other paths that where written>,
  "<path to opensim_install_folder/opensim-core>",
  "<path to opensim_install_folde/opensim_core>/dependencies/spdlog/include",
  "<path to opensim_install_folde/opensim_core>/Vendors/lepton/include"
]
~~~