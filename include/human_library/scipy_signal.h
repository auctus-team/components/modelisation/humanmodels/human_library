#ifndef scipy_signal_h
#define scipy_signal_h

#include <sstream>
#include <iostream>
#include <cmath>
#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <string>
#include <complex>


using namespace std;

namespace scipy_signal
{
    extern double eps;

    extern std::map<std::string,std::string> band_dict;
    extern std::map<std::string,std::string> filter_dict;
    extern std::map<int,std::string> error_type;

    int filtfilt(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::MatrixXd x,        
        Eigen::MatrixXd & y,
        int axis=0,
        std::string padtype="odd",
        int padlen=-1,
        std::string method = "pad",
        int irlen = -1
    );

    int butter(int N, 
    Eigen::VectorXd Wn, 
    Eigen::MatrixXd & a,
    Eigen::MatrixXd & b,    
    std::string btype="low", 
    bool analog=false, 
    double fs= nan(""));

    int lfilter(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::MatrixXd x,
        Eigen::VectorXd zi,
        Eigen::MatrixXd & y
    );

    int lfilter_zi(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::VectorXd & zi
    );

    int even_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    );

    int const_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    );

    int odd_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    );

    void axis_slice(
        Eigen::MatrixXd a,
        Eigen::MatrixXd & out,
        int start = 0,
        int stop = INFINITY,
        int step = 1,
        int axis = 0
    );

    int _validate_pad(
        std::string padtype,
        int padlen,
        Eigen::MatrixXd x,
        int axis, 
        int ntaps,
        int & edge,
        Eigen::MatrixXd & ext
    );

    int iirfilter(int N, 
    Eigen::VectorXd Wn,
    std::vector<Eigen::MatrixXcd> & outRepres, 
    double rp = nan(""),
    double rs = nan(""),
    std::string btype = "band",
    bool analog = false,
    std::string ftype="butter",
    std::string output="ba",
    double fs = nan("") );

    void lower(std::string & s);

    int buttap(int N, Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k);

    int lp2lp_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double wo=1.0);

    int lp2hp_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double wo=1.0);

    //int lp2bp_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double wo=1.0, double bw = 1.0);

    int _relative_degree(Eigen::MatrixXcd z, Eigen::MatrixXcd p, int & degree);

    int bilinear_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double fs);

    int zpk2tf( Eigen::MatrixXcd z, Eigen::MatrixXcd p, Eigen::MatrixXcd k,Eigen::MatrixXcd & b, Eigen::MatrixXcd & a, bool & ret_complexe_conj);

    void poly(Eigen::MatrixXcd z, Eigen::MatrixXcd & b);

}

#endif